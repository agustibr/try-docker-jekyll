---
layout: home
tiles-source: posts
tiles-count: 4

title: HomePage
landing-title: "HomePage title"
description: "Home description text ..."
image: images/homepage.jpg
banner_cta:
  text: Contact us! 
  href: "#contact"
section_cta:
  text: "read our posts" 
  href: "/posts"
---

This is the home page of a `Jekyll` + `Docker` static site.
