---
layout: post
title: Playing with Jekyll and Docker
categories: [code]
tags: [coding, docker, jekyllrb, "101"]
---


## Install Docker Engine

### Set up the repository

Update the apt package index:

	sudo apt-get update

Install packages to allow apt to use a repository over HTTPS:

	sudo apt-get install \
          apt-transport-https \
          ca-certificates \
          curl \
          gnupg-agent \
          software-properties-common

Add Docker’s official GPG key:

	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

Use the following command to set up the `stable` repository.

```
sudo add-apt-repository \
"deb [arch=amd64] https://download.docker.com/linux/ubuntu \
$(lsb_release -cs) \
stable"
```

### Install Docker Engine - Community

Update the apt package index.

    sudo apt-get update

Install the latest version of Docker Engine - Community and containerd, or go to the next step to install a specific version:

    sudo apt-get install docker-ce docker-ce-cli containerd.io

Verify that Docker Engine - Community is installed correctly by running the hello-world image.

	sudo docker run hello-world

### Upgrade Docker Engine - Community

To upgrade Docker Engine - Community, first run `sudo apt-get update`, then follow the installation instructions, choosing the new version you want to install.

### Post-installation steps for Linux

### Manage Docker as a non-root user

If you don’t want to preface the docker command with sudo, create a Unix group called docker and add users to it.

To create the docker group and add your user:

Create the docker group.

	sudo groupadd docker

Add your user to the docker group.

	sudo usermod -aG docker $USER

Log out and log back in so that your group membership is re-evaluated.  
On Linux, you can also run the following command to activate the changes to groups:

	newgrp docker

Verify that you can run docker commands without sudo.

	docker run hello-world

This command downloads a test image and runs it in a container. When the container runs, it prints an informational message and exits.

### Configure Docker to start on boot

Most current Linux distributions (RHEL, CentOS, Fedora, Ubuntu 16.04 and higher) use systemd to manage which services start when the system boots. Ubuntu 14.10 and below use upstart.

For `systemd` run:

	sudo systemctl enable docker

To disable this behavior, use disable instead.

	sudo systemctl disable docker

## Install Compose

Run this command to download the current stable release of Docker Compose:

	sudo curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

Apply executable permissions to the binary:

	sudo chmod +x /usr/local/bin/docker-compose

Test the installation.

	docker-compose --version

## Running Jekyll locally with Docker

The rest of this post assumes you have Docker and Docker Compose installed and have some basic knowledge of working with it. Docker has some great getting started guides for to [Develop with Docker](https://docs.docker.com/develop/).

The easiest way to get started is probably to use Jekyll with a Gemfile.

### Setup a Basic Jekyll site

We’ll start with a basic Jekyll site consisting of three files:

```
├── _config.yml
├── Gemfile
└── index.md
```

Create `Gemfile` in the root with the following:

```
source 'https://rubygems.org'

gem 'jekyll'

gem 'fortyone-jekyll-theme'
```

Then the `_config.yml` with the contents:

```
name: My Jekyll Website
theme: fortyone-jekyll-theme
```

And the `index.md`:

```
---
title: My page
layout: home
---

# {{ page.title }}

Content is written in [Markdown](https://learnxinyminutes.com/docs/markdown/). Plain text format allows you to focus on your **content**.

```

### Dockerize the site

To use the Jekyll image without having to pass in the required options every time to create a container, we can make use of Docker Compose. Create a file called `docker-compose.yml` with these contents:

```
jekyll:
    image: jekyll/jekyll
    command: jekyll serve --watch --incremental
    ports:
        - 4000:4000
    volumes:
        - .:/srv/jekyll
```

### Start the docker jekyll

That’s all! You can start your Jekyll site by browsing to your site’s directory using a terminal and doing

```
docker-compose up
```

And go to [http://0.0.0.0:4000](http://0.0.0.0:4000)!


## Source code

You can find it here: https://gitlab.com/agustibr/try-docker-jekyll

## Deep Dive

* [Compose file version 3 reference : Docker Documentation](https://docs.docker.com/compose/compose-file/)
* [Overview of docker-compose CLI : Docker Documentation](https://docs.docker.com/compose/reference/overview/)
* [labs/webapps.md at master · docker/labs · GitHub](https://github.com/docker/labs/blob/master/beginner/chapters/webapps.md)

---

via:

* [Get Docker Engine - Community for Ubuntu | Docker Documentation](https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-engine---community-1)
* [Install Docker Compose | Docker Documentation](https://docs.docker.com/compose/install/)
* [Running Jekyll locally with Docker · MOIST](https://kristofclaes.github.io/2016/06/19/running-jekyll-locally-with-docker/)
* [Convert an HTML site to Jekyll | Jekyll • Simple, blog-aware, static sites](https://jekyllrb.com/tutorials/convert-site-to-jekyll/)
* [Deployment | Jekyll • Simple, blog-aware, static sites](https://jekyllrb.com/docs/step-by-step/10-deployment/#gemfile)
